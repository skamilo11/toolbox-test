# toolbox-test

Prueba de aplicación

Para correr la aplicación se debe estar dentro de la carpeta **toolbox-test** y correr en consola el comando:
	`docker-compose up --build`
Luego en el navegador ingresar a 
	**http://localhost/**

En la pantalla se verá un cuadro de texto con **texto...** en este se debe escribir lo que desea enviar.
Si la respuesta es correcta se mostrará una tarejta abajo con el texto enviado.

* Para realizar las pruebas del backend se debe abrir una terminal en **toolbox-test/back** y dentro de esta
ejecutar primero `npm i` para instalar las dependencias y luego `npm test`
 
* Para realizar las pruebas del front se debe abrir una terminal en **toolbox-test/fron**t y dentor de estaa
ejecutar primero `npm i` para instalar las dependencias, luego `npm test` y finalmente `a` para ejecutar  
todos los test
