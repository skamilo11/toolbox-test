import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import {ROOT} from '../constants/paths';
import HOME from '../containers/Home';


const AppRouter =  () => {
    return <BrowserRouter>
        <Route path={ROOT} component={HOME} />
    </BrowserRouter>
}

export default AppRouter;