import axios from 'axios';
const url = 'http://localhost:3000/';

export default {
    get: (resource, success, failure) => {
        axios
            .get(`${url}${resource}`)
            .then(res => success(res))
            .catch(err => failure(err));
    }
};
