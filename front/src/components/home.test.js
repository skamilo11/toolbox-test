import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Home from './home';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

configure({ adapter: new Adapter() });

describe('<Home />', () => {

    let wrapper;

    beforeEach(() => {
       wrapper = shallow(<Home />);
    });
    it('should show render one Card element if there is not a text in the state', () => {
        expect(wrapper.find(Card)).toHaveLength(1);
    });

    it('should show render two Cards elements if there is text in the state', () => {
        wrapper.setProps({ textFromServer: 'Prueba' });
        expect(wrapper.find(Card)).toHaveLength(2);
    });

    it('should show render one Card component with one Card Content component inside', () => {
        wrapper.setProps({ textFromServer: 'Prueba' });
        expect(wrapper.find(CardContent)).toHaveLength(2);
    });
});

