import React, { Fragment, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, Grid, TextField } from '@material-ui/core';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles({
    card: {
        minWidth: 275,
        maxWidth: 300,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    grid: {
        justifyContent: 'center',
        direction: 'columns',
        minHeight: '100vh'
    }
});

const Home = ({ getText, textFromServer }) => {
    const classes = useStyles();

    const [text, setText] = useState('');

    const handleChange = (event) => setText(event.target.value);

    const handleClick = () => getText(text);

    return (
        <Fragment>
            <Grid
                container
                className={classes.grid}
            >
                <Grid item xs={3}>
                    <Card className={classes.card}>
                        <CardContent>
                            <Typography variant="h5" component="h2">
                                Obtener texto
                        </Typography>
                        </CardContent>
                        <CardActions>
                            <TextField id="standard-basic" label="Texto..." value={text} onChange={handleChange} />
                            <Button size="small" onClick={handleClick}>Mostrar</Button>
                        </CardActions>
                    </Card>
                    {textFromServer ? <Card className={classes.card}>
                        <CardContent>
                            <Typography variant="h5" component="h2">
                                El texto es {textFromServer}
                            </Typography>
                        </CardContent>
                    </Card>:null}
                </Grid>
            </Grid>
        </Fragment>
    );

}

export default Home;
