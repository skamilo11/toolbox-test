import reducer from './text';
import {REQUEST_TEXT, SUCCESS_TEXT, ERROR_TEXT} from './text';

describe('text reducer', () =>{
    it('should return the initial state',() => {
        expect(reducer(undefined,{})).toEqual({
            error: undefined,
            text: undefined,
            fetching: false,
        });
    });

    it('should return the initial state',() => {
        expect(reducer(undefined,{type: REQUEST_TEXT})).toEqual({
            error: undefined,
            text: undefined,
            fetching: true,
        });
    });

    it('should return the initial state',() => {
        expect(reducer(undefined,{type: ERROR_TEXT, error: 'Test error'})).toEqual({
            error: 'Test error',
            text: undefined,
            fetching: false,
        });
    });

    it('should return the initial state',() => {
        expect(reducer(undefined,{type: SUCCESS_TEXT, text: 'Test text'})).toEqual({
            error: undefined,
            text: 'Test text',
            fetching: false,
        });
    });
});