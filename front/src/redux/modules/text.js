import request from '../../api';

export const REQUEST_TEXT = 'TOOLBOX-TEXT/TEXT/REQUEST_TEXT';
export const SUCCESS_TEXT = 'TOOLBOX-TEXT/TEXT/SUCCESS_TEXT';
export const ERROR_TEXT = 'TOOLBOX-TEXT/TEXT/ERROR_TEXT';

const inititalState = {
    error: undefined,
    text: undefined,
    fetching: false,
};

const reducer = (state = inititalState, action) => {
    switch (action.type) {
        case REQUEST_TEXT:
            return {
                ...state,
                fetching: true
            };
        case SUCCESS_TEXT:
            return {
                ...state,
                fetching: false,
                text: action.text
            };
        case ERROR_TEXT:
            return {
                ...state,
                error: action.error
            };
        default:
            return state;
    }
};

export const getText = inputText => dispatch => {
    dispatch({
        type: REQUEST_TEXT
    });
    request.get(`text/${inputText}`,
        (res) =>
            dispatch({
                type: SUCCESS_TEXT,
                text: res.data.text
            })
        ,
        (err) => {
            dispatch({
                type: ERROR_TEXT,
                error: err.message
            })
        }
    );

};

export default reducer;
