import { combineReducers } from 'redux';
import text from './modules/text';

export default combineReducers({
    text,
});