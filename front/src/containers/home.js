import { connect } from 'react-redux';
import { getText } from '../redux/modules/text';
import Home from '../components/Home';

const mapStateToProps = (state) => ({
    textFromServer: state.text.text,
});


const mapDispatchToProps = {
    getText
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
