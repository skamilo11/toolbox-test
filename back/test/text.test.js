'use strict';

const app = require('../server'),
  chai = require('chai'),
  request = require('supertest');

  const expect = chai.expect;

  const testText = 'texto de prueba';

  describe('Get Text API tests', function() {
    describe('#GET / text', function() { 
      it('Shoult get the same text', function(done) { 
        request(app) .get(`/text/${testText}`)
          .end(function(err, res) { 
            expect(res.statusCode).to.equal(200); 
            expect(res.body).to.be.an('object'); 
            expect(res.body.text).to.be.an('string'); 
            expect(res.body.text).eqls('texto de prueba'); 
            done(); 
          }); 
      });
    });

    describe('#GET / text', function() { 
        it('Shoult get 404 code', function(done) { 
          request(app) .get(`/text/`)
            .end(function(err, res) { 
              expect(res.statusCode).to.equal(404);
              done(); 
            }); 
        });
      });
    
  });