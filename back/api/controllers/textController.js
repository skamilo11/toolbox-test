'use strict';

exports.getText = (req, res) => {
    if (req.params.text)
        res.json({'text':req.params.text});
    else
        res.send('El parámetro es obligatorio');
}