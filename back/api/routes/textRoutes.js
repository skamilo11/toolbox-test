'use strict';
const textControler = require('../controllers/textController')

module.exports = (app) => {
    app.route('/text/:text').get(textControler.getText);
    app.use((req, res) =>
        res.status(404).send(`${req.originalUrl} not found`)
    );
}