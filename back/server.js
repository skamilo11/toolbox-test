'use strict';

const express = require('express');
const app = express();
const routes = require('./api/routes/textRoutes');
const port = 3000
const bodyParser = require('body-parser');

app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

routes(app);


app.listen(port, () => console.log(`Servidor corriendo en el puerto ${port}!`));

module.exports = app;

